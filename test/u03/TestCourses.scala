package u03

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TestCourses {
  import Lists.List
  import List._
  import Courses._
  import u02.Modules.Person
  import u02.Modules.Person._

  val people: List[Person] =  Cons(Teacher("Mirko Viroli", "SVILUPPO DEI SISTEMI SOFTWARE"),
                  Cons(Student("Mario Rossi", 2020),
                    Cons(Teacher("Alessandro Ricci", "PROGRAMMAZIONE CONCORRENTE E DISTRIBUITA"),
                      Cons(Student("Giulia Verdi", 2019), Nil()))))
  val students: List[Person] = Cons(Student("Mario Rossi", 2020), Cons(Student("Giulia Verdi", 2019), Nil()))

  @Test def testCourses(): Unit ={
    assertEquals(Cons("SVILUPPO DEI SISTEMI SOFTWARE", Cons("PROGRAMMAZIONE CONCORRENTE E DISTRIBUITA", Nil())), getCourses(people))
    assertEquals(Nil(), getCourses(students))
  }

  @Test def testCourses2(): Unit ={
    assertEquals(Cons("SVILUPPO DEI SISTEMI SOFTWARE", Cons("PROGRAMMAZIONE CONCORRENTE E DISTRIBUITA", Nil())), getCourses2(people))
    assertEquals(Nil(), getCourses2(students))
  }
}
