package u03

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class StreamTest {
  import Lists.List._
  import Streams.Stream
  import MyStream._

  val s: Stream[Int] = Stream.take(Stream.iterate(0)(_+1))(10)

  @Test def testDrop(): Unit ={
    assertEquals(Cons(6, Cons(7, Cons(8, Cons(9, Nil())))), Stream.toList(drop(s)(6)))
    assertEquals(s, drop(s)(0))
  }

  @Test def testConstant(): Unit ={
    assertEquals(Cons("x", Cons("x", Cons("x", Cons("x", Cons("x", Nil()))))), Stream.toList(Stream.take(constant("x"))(5)))
  }
}
