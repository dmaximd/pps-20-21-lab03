package u03

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class FoldTest {
  import Fold._
  import Lists.List
  import List._

  val lst: List[Int] = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))

  @Test def testFoldLeft(): Unit ={
    assertEquals(-16, foldLeft(lst)(0)(_-_))
  }

  @Test def testFoldRight(): Unit ={
    assertEquals(-8, foldRight(lst)(0)(_-_))
  }

  @Test def testOptimizedFoldRight(): Unit ={
    assertEquals(-8, foldRightOptimized(lst)(0)(_-_))
  }
}
