package u03

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class ListTest {

  import ExtendedList._
  import Lists.List
  import List._
  import u02.Optionals.Option

  val lst: List[Int] = Cons(10, Cons(20, Cons(30, Nil())))
  val decrList: List[Int] = Cons(30, Cons(20, Cons(10, Nil())))

  @Test def testDrop(): Unit ={
    assertEquals(Cons(20, Cons(30, Nil())), drop(lst, 1))
    assertEquals(Cons(30, Nil()), drop(lst, 2))
    assertEquals(Nil(), drop(lst, 5))
    assertEquals(lst, drop(lst, 0))
  }

  @Test def testFlatMap(): Unit ={
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), flatMap(lst)(v => Cons(v+1, Nil())))
    assertEquals(Cons(11, Cons(12, Cons(21, Cons(22, Cons(31, Cons(32, Nil())))))), flatMap(lst)(v => Cons(v+1, Cons(v+2, Nil()))))
  }

  @Test def testMap(): Unit ={
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), mapWithFlat(lst)(_+1))
    assertEquals(Cons(false, Cons(false, Cons(true, Nil()))), mapWithFlat(lst)(_>20))
  }

  @Test def testFilter(): Unit ={
    assertEquals(Cons(20, Cons(30, Nil())), filterWithFlat(lst)(_>15))
    assertEquals(Nil(), filterWithFlat(lst)(_<5))
  }

  @Test def testMax(): Unit ={
    assertEquals(Option.Some(30), max(lst))
    assertEquals(Option.None(), max(Nil()))
    assertEquals(Option.Some(30), max(decrList))
  }
}
