package u03

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import u03.Lists.List._

class FibonacciTest {
  import Streams.Stream
  import Fibonacci._

  val fibs: Stream[Int] = fibonacci()

  @Test def testFibonacci(): Unit ={
    assertEquals(Cons(0, Cons(1, Cons(1, Cons(2, Cons(3, Cons(5, Cons(8, Cons(13, Nil())))))))), Stream.toList(Stream.take(fibs)(8)))
    assertEquals(Cons(0, Nil()), Stream.toList(Stream.take(fibs)(1)))
  }
}
