package u03

import scala.annotation.tailrec

object Fold {
  import Lists.List
  import List._

  @tailrec
  def foldLeft[A, B](list: List[A])(base: B)(op: (B, A) => B): B = list match {
    case Cons(h, t) => foldLeft(t)(op(base, h))(op)
    case Nil() => base
  }

  def foldRight[A, B](list: List[A])(base: B)(op: (A, B) => B): B = list match {
    case Cons(h, t) => op(h, foldRight(t)(base)(op))
    case Nil() => base
  }

  def foldRightOptimized[A, B](list: List[A])(base: B)(op: (A, B) => B): B = {
    def revert(l: List[A]): List[A] = l match {
      case Cons(h, t) => append(revert(t), Cons(h, Nil()))
      case _ => Nil()
    }
    @tailrec
    def executeFold(lst: List[A])(base: B)(op: (A, B) => B): B = lst match {
      case Cons(h, t) => executeFold(t)(op(h, base))(op)
      case Nil() => base
    }
    executeFold(revert(list))(base)(op)
  }
}
