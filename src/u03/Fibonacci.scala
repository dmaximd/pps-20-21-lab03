package u03

object Fibonacci {
  import Streams.Stream
  import Stream._

  def fibonacci(): Stream[Int] = {
    def fib(it: Int)(v1: => Int, v2: => Int): Stream[Int] = it match {
      case 0 | 1 => cons(it, fib(it + 1)(0, 1))
      case _ => cons(v1 + v2, fib(it + 1)(v2, v1 + v2))
    }
    fib(0)(0, 0)
  }
}
