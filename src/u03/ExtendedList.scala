package u03

import scala.annotation.tailrec

object ExtendedList {

  import Lists.List
  import List._
  import u02.Optionals.Option
  import Option._

  @tailrec
  def drop[A](list: List[A], amount: Int): List[A] = (list, amount) match {
    case (Cons(_, t), amount) if amount > 0 => drop(t, amount-1)
    case _ => list
  }

  def flatMap[A, B](list: List[A])(f: A => List[B]): List[B] = list match{
    case Cons(h, t) => append(f(h), flatMap(t)(f))
    case Nil() => Nil()
  }

  def mapWithFlat[A, B](list: List[A])(f: A => B): List[B] = flatMap(list)(a => Cons(f(a), Nil()))

  def filterWithFlat[A](list: List[A])(pred: A => Boolean): List[A] = flatMap(list)({
    case a if pred(a) => Cons(a, Nil())
    case _ => Nil()
  })

  def max(list: List[Int]): Option[Int] = {
    @tailrec
    def matchMax(l: List[Int], max: Option[Int]): Option[Int] = l match {
      case Cons(h, t) if h < getOrElse(max, h) => matchMax(t, Some(getOrElse(max, h)))
      case Cons(h, t) if h >= getOrElse(max, h) => matchMax(t, Some(h))
      case Nil() => max
    }
    matchMax(list, None())
  }
}
