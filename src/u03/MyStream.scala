package u03

import scala.annotation.tailrec

object MyStream {
  import Lists.List
  import List._
  import Streams.Stream
  import Stream._

  @tailrec
  def drop[A](stream: Stream[A])(n: Int): Stream[A] = {
    def toStream(list: List[A]): Stream[A] = list match{
      case Cons(h, t) => cons(h, toStream(t))
      case Nil() => empty()
    }
    (Stream.toList(stream), n) match {
      case (Cons(_, t), n) if n > 0 => drop(toStream(t))(n - 1)
      case _ => stream
    }
  }

  def constant[A](v: A): Stream[A] = cons(v, constant(v))
}
