package u03

object Courses {
  import Lists.List
  import List._
  import ExtendedList._
  import u02.Modules.Person
  import Person._

  def getCourses(people: List[Person]): List[String] = {
    val predicate: Person => Boolean = {
      case Teacher(_, _) => true
      case _ => false
    }
    mapWithFlat( filterWithFlat(people)(predicate) )({
      case Teacher(_, c) => c
    })
  }

  def getCourses2(people: List[Person]): List[String] = {
    val filter: Person => List[String] = {
      case Teacher(_, c) => Cons(c, Nil())
      case _ => Nil()
    }
    flatMap(people)(filter)
  }
}
